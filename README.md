# SwaggerServerPrototype
 
This is a simple prototype illustrating how to publish a distributed Swagger documentation with correct mime types on a http server using the very cool and minimal http server Java framework called Spark (http://sparkjava.com/).

## To test you need:
* Maven 3
* Java SE 8 (jdk)
* (Eclipse)

## Moving parts
* This is currently a "fulhack" illustrating a point, this can be fixxed to be general and potentially useful :)
* Just follow the action in `App.java` 
 `SwaggerServerPrototype / src / main / java / com / rl / swagger / App.java`
* The different json files are stored in project root (=fulhack)
* The App exposes the following structure (with the resp. json files):
```
http://localhost:4567/main
http://localhost:4567/main/pet
http://localhost:4567/main/user
http://localhost:4567/main/store
```