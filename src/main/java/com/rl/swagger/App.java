package com.rl.swagger;

import static spark.Spark.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class App {
    public static void main(String[] args) {
    	getFile("main");
    	get("/main", (request, response) -> {
            response.type("application/json");
            return getFile("main.json");
         });
    	get("/main/pet", (request, response) -> {
            response.type("application/json");
            return getFile("pet.json");
         });
    	get("/main/user", (request, response) -> {
            response.type("application/json");
            return getFile("user.json");
         });
    	get("/main/store", (request, response) -> {
            response.type("application/json");
            return getFile("store.json");
         });
    	
    }

	private static String getFile(String s) {
		String content="";
		try {
			content = new String(Files.readAllBytes(Paths.get(s)));
			System.out.println(content);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return content;
	}
	
}